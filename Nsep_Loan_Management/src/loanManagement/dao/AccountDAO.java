/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : AccountDAO.java
Creation Date : 13/12/2011
History : Initial Creation
*/





package loanManagement.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import loanManagement.beans.Loan_Account;
import loanManagement.beans.Loan_Repayment_Details;
import loanManagement.beans.Loan_Repayment_Schedule;
import loanManagement.beans.Loan_Requests;
import loanManagement.util.GenericConnection;

import org.apache.log4j.Logger;

public class AccountDAO {
	static Logger logger = Logger.getLogger(AccountDAO.class);
	Connection conn = null;
    ResultSet rs = null;
    Statement st=null;
    GenericConnection gc=null;
    PreparedStatement pr=null;
    
    /* 
      Parameter : No parameters
      Return Type : ArrayList<Loan_Requests>
      Since: JDK 1.5.0
      See: getConnection(),createStatement(),executeQuery(), getInt(), getString()
    */
    // method to generate all pending loan requests and displays schedule
    public ArrayList<Loan_Requests> requestDetails()
    {
    	ArrayList<Loan_Requests> requests=new ArrayList<Loan_Requests>();
    	 
    	gc=new GenericConnection();
    	try
    	{
    	  conn=gc.getConnection();	                                  
		    st = conn.createStatement() ;
		    rs = st.executeQuery("select * from LM_Loan_Requests where status='pending' and schedule_generated ='yes'");
		    while(rs.next())
		    {
		    	Loan_Requests lr=new Loan_Requests();
		    	lr.setLoan_Request_No(rs.getString("Loan_Request_No"));
		    	lr.setStudent_ID(rs.getInt("Student_ID"));
		    	lr.setLoan_Amount(rs.getInt("Loan_Amount"));
		    	lr.setPurpose_Of_Loan(rs.getString("Purpose_Of_Loan"));
		    	requests.add(lr);
		    }
		     
    	}
		    catch (Exception e) 
	      	{		  
	      	e.printStackTrace();
	    	  logger.error(" Exception Occurred in AccountDAO",e);
	  		try {
	  			conn.rollback();
	  		} catch (SQLException sqle) {
	  			logger.error(" SQLException while roll back in AccountDAO",e);
	  		}
	  	   } finally {
	  		// Cleanup Resources
	  		try {
	  			if(rs != null)
	  				gc.closeResultSet(rs);
	  			if(st != null)
	  				gc.closeStatement(st);
	  			if(conn != null)
	  				gc.closeConnection(conn);
	  		} catch (Exception e) {
	  			logger.error(" Exception while cleaning up resources in AccountDAO",e);
	  		}
	      }
	  	   
	  	   
	  	   return requests;
	      
    }
    
    /* 
    Parameter : Loan request number
    Return Type : void
    Since: JDK 1.5.0
    See: getConnection(),setAutoCommit(), createStatement(),executeUpdate()
  */
  
    
    // method updates databases if user accepts the schedule
      public void updateRequestStatus(String lnreqno)
      {

      	gc=new GenericConnection();
      	try
      	{
      	    conn=gc.getConnection();	
      	    conn.setAutoCommit(false) ;                                    
  		    st = conn.createStatement() ;
  		    String sql=null;
            
  		    sql="update LM_Loan_Requests set status='accepted' where LOAN_REQUEST_NO='"+lnreqno+"'";
          	st.executeUpdate(sql);
    			conn.commit();
            
        	}
        	  catch (Exception e) 
          	{
       		  
          	e.printStackTrace();
        	  logger.error(" Exception Occurred in AccountDAO ",e);
      		try {
      			conn.rollback();
      		} catch (SQLException sqle) {
      			logger.error(" SQLException while roll back in AccountDAO",e);
      		}
      	   } finally {
      		// Cleanup Resources
      		try {
      			if(rs != null)
      				gc.closeResultSet(rs);
      			if(st != null)
      				gc.closeStatement(st);
      			if(conn != null)
      				gc.closeConnection(conn);
      		} catch (Exception e) {
      			logger.error(" Exception while cleaning up resources in AccountDAO",e);
      		}
          }  
      }
      
      /* 
      Parameter : Loan_Account
      Return Type : void
      Since: JDK 1.5.0
      See: getConnection(),setAutoCommit(), createStatement(),executeUpdate()
    */
    
      // method to create loan account, takes an object of bean Loan_Account as parameter
      public String createLoanAccount(Loan_Account la)
      {
        int nextvalue=0;
      	gc=new GenericConnection();
      	try
      	{
      	    Date dt=null;
      		conn=gc.getConnection();	
      	    conn.setAutoCommit(false) ;                                    
  		    st = conn.createStatement() ;
  		    String sql=null;
            sql="select sysdate from dual";
            rs=st.executeQuery(sql);
            rs.next();
             dt=rs.getDate(1);
            
            rs = st.executeQuery("select loan_generator.NEXTVAL from dual" ) ;
		    rs.next() ;
		    nextvalue = rs.getInt(1) ;
  		    sql="insert into LM_Loan_Account values(?,?,?,?,?)";
  		    pr=conn.prepareStatement(sql);
  		    pr.setString(1,"LNNUM"+nextvalue);
  		    pr.setInt(2, la.getStudent_ID());
  		    pr.setString(3, la.getLoan_Request_No());
  		    pr.setInt(4,la.getLoan_Amount());
  		    pr.setDate(5, dt);
  		     
          	pr.executeUpdate();
    			conn.commit();
             
        	}
        	  catch (Exception e) 
          	{
       		  
          	e.printStackTrace();
        	  logger.error(" Exception Occurred in AccountDAO ",e);
      		try {
      			conn.rollback();
      		} catch (SQLException sqle) {
      			logger.error(" SQLException while roll back in AccountDAO",e);
      		}
      	   } finally {
      		// Cleanup Resources
      		try {
      			if(rs != null)
      				gc.closeResultSet(rs);
      			if(st != null)
      				gc.closeStatement(st);
      			if(conn != null)
      				gc.closeConnection(conn);
      		} catch (Exception e) {
      			logger.error(" Exception while cleaning up resources in AccountDAO",e);
      		}
          }  
      	 return "LNNUM"+nextvalue;
      }
      
      
      /* 
      Parameter : Loan Request Number
      Return Type : void
      Since: JDK 1.5.0
      See: getConnection(),setAutoCommit(), createStatement(),executeUpdate()
    */
      //method to delete loan request 
      public void deleteRequest(String lnreqno)
      {

      	gc=new GenericConnection();
      	try
      	{
      	    conn=gc.getConnection();	
      	    conn.setAutoCommit(false) ;                                    
  		    st = conn.createStatement() ;
  		    String sql=null;
            sql="delete from LM_Repayment_Schedule where LOAN_REQUEST_NO='"+lnreqno+"'";
            st.executeUpdate(sql);
			conn.commit();
  		    sql="delete from LM_Loan_Requests where LOAN_REQUEST_NO='"+lnreqno+"'";
          	st.executeUpdate(sql);
    			conn.commit();
            
        	}
        	  catch (Exception e) 
          	{
       		  
          	e.printStackTrace();
        	  logger.error(" Exception Occurred in AccountDAO ",e);
      		try {
      			conn.rollback();
      		} catch (SQLException sqle) {
      			logger.error(" SQLException while roll back in AccountDAO",e);
      		}
      	   } finally {
      		// Cleanup Resources
      		try {
      			if(rs != null)
      				gc.closeResultSet(rs);
      			if(st != null)
      				gc.closeStatement(st);
      			if(conn != null)
      				gc.closeConnection(conn);
      		} catch (Exception e) {
      			logger.error(" Exception while cleaning up resources in AccountDAO",e);
      		}
          }  
      }
      
      /* 
      Parameter : Loan Number
      Return Type : ArrayList<Loan_Repayment_Details>
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      //method to retrieve Loan repayment details for a corresponding loanid
      public ArrayList<Loan_Repayment_Details> repaymentDetails(String loanid)
      {
      	ArrayList<Loan_Repayment_Details> history=new ArrayList<Loan_Repayment_Details>();
      	 
      	gc=new GenericConnection();
      	try
      	{
      	  conn=gc.getConnection();	                                  
  		    st = conn.createStatement() ;
  		    rs = st.executeQuery("select * from LM_Repayment_Details where Loan_Account_No='"+loanid+"'");
  		    while(rs.next())
  		    {
  		    	Loan_Repayment_Details lr=new Loan_Repayment_Details();
  		    	lr.setAmount(rs.getInt("Amount"));
  		    	lr.setDeposit_Date(rs.getDate("Deposit_Date"));
  		    	lr.setFine(rs.getInt("Fine"));
  		    	lr.setLoan_Account_No(loanid);
  		    	history.add(lr);
  		    }
  		     
      	}
  		    catch (Exception e) 
  	      	{		  
  	      	e.printStackTrace();
  	    	  logger.error(" Exception Occurred in AccountDAO",e);
  	  		try {
  	  			conn.rollback();
  	  		} catch (SQLException sqle) {
  	  			logger.error(" SQLException while roll back in AccountDAO",e);
  	  		}
  	  	   } finally {
  	  		// Cleanup Resources
  	  		try {
  	  			if(rs != null)
  	  				gc.closeResultSet(rs);
  	  			if(st != null)
  	  				gc.closeStatement(st);
  	  			if(conn != null)
  	  				gc.closeConnection(conn);
  	  		} catch (Exception e) {
  	  			logger.error(" Exception while cleaning up resources in AccountDAO",e);
  	  		}
  	      }
  	  	   
  	  	   
  	  	   return history;
  	      
      }
      
      /* 
      Parameter : Loan number
      Return Type : String
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      
      //method to check loanid exists in the database or not
      public String checkLoanID(String loanid)
      {
      	String result=null;
      	 
      	gc=new GenericConnection();
      	try
      	{
      	  conn=gc.getConnection();	                                  
  		    st = conn.createStatement() ;
  		    rs = st.executeQuery("select * from LM_LOAN_ACCOUNT where Loan_Account_No='"+loanid+"'");
  		    int i=0;
  		    while(rs.next())
  		    	i++;
  		    if(i==0)
  		    	result="no";
  		    else
  		    	result="yes";
      	}
  		    catch (Exception e) 
  	      	{		  
  	      	e.printStackTrace();
  	    	  logger.error(" Exception Occurred in LoanDAO ",e);
  	  		try {
  	  			conn.rollback();
  	  		} catch (SQLException sqle) {
  	  			logger.error(" SQLException while roll back in LoanDAO",e);
  	  		}
  	  	   } finally {
  	  		// Cleanup Resources
  	  		try {
  	  			if(rs != null)
  	  				gc.closeResultSet(rs);
  	  			if(st != null)
  	  				gc.closeStatement(st);
  	  			if(conn != null)
  	  				gc.closeConnection(conn);
  	  		} catch (Exception e) {
  	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
  	  		}
  	      }
  	  	   
  	  	   
  	  	   return result;
  	      
      }
      
      /* 
      Parameter : Loan Number
      Return Type : String
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      //method to view status of current month payment
      public String currentMonthRepaymentStatus(String loanid)
      {
      	String result="unpaid";
      	 
      	gc=new GenericConnection();
      	try
      	{
      	  conn=gc.getConnection();	                                  
  		    st = conn.createStatement() ;
  		    rs=st.executeQuery("select sysdate from dual");
  		    rs.next();
            Date today=rs.getDate(1);
  		    rs = st.executeQuery("select Deposit_Date from LM_Repayment_Details where Loan_Account_No='"+loanid+"'");
  		    int i=0;
  		    while(rs.next())
  		    {
  		    	Date dt=rs.getDate(1);
  		        if(dt.getMonth()==today.getMonth() && dt.getYear()==today.getYear())
  		        {
  		        	result="paid";
  		        	break;
  		        }
  		    }
      	}
  		    catch (Exception e) 
  	      	{		  
  	      	e.printStackTrace();
  	    	  logger.error(" Exception Occurred in LoanDAO ",e);
  	  		try {
  	  			conn.rollback();
  	  		} catch (SQLException sqle) {
  	  			logger.error(" SQLException while roll back in LoanDAO",e);
  	  		}
  	  	   } finally {
  	  		// Cleanup Resources
  	  		try {
  	  			if(rs != null)
  	  				gc.closeResultSet(rs);
  	  			if(st != null)
  	  				gc.closeStatement(st);
  	  			if(conn != null)
  	  				gc.closeConnection(conn);
  	  		} catch (Exception e) {
  	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
  	  		}
  	      }
  	  	   
  	  	   
  	  	   return result;
  	      
      }
      
      /* 
      Parameter : Loan Number
      Return Type : String
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      //method to check whether all installments of the loan are paid or not
      public String ifAllInstallmentsPaid(String loanid)
      {
    	  String result=null;
       	 
        	gc=new GenericConnection();
        	try
        	{
        	  conn=gc.getConnection();	                                  
    		    st = conn.createStatement() ;
    		    rs = st.executeQuery("select count(Loan_Account_No) from LM_Repayment_Details where Loan_Account_No='"+loanid+"' group by Loan_Account_No");
    		    int count1=0; 
    		    while(rs.next())
    		    count1=rs.getInt(1);
    		    rs = st.executeQuery("select count(s.Student_ID) from LM_Loan_Account a , LM_Repayment_Schedule s where s.Student_ID=a.Student_ID and Loan_Account_No='"+loanid+"' group by s.Student_ID");
    		    int count2=0; 
    		    while(rs.next())
    		    count2=rs.getInt(1);
    		    if(count1==count2)
    		    	result="yes";
    		    else
    		    	result="no";
        	}
    		    catch (Exception e) 
    	      	{		  
    	      	e.printStackTrace();
    	    	  logger.error(" Exception Occurred in LoanDAO ",e);
    	  		try {
    	  			conn.rollback();
    	  		} catch (SQLException sqle) {
    	  			logger.error(" SQLException while roll back in LoanDAO",e);
    	  		}
    	  	   } finally {
    	  		// Cleanup Resources
    	  		try {
    	  			if(rs != null)
    	  				gc.closeResultSet(rs);
    	  			if(st != null)
    	  				gc.closeStatement(st);
    	  			if(conn != null)
    	  				gc.closeConnection(conn);
    	  		} catch (Exception e) {
    	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
    	  		}
    	      }
    	  	   
    	  	   
    	  	   return result;
    	      
      }
      
      /* 
      Parameter : Loan Number
      Return Type : int
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      //method to get studentid using loanid
      public int getStudentID(String loanid)
      {
      	int studentId=0;
      	 
      	gc=new GenericConnection();
      	try
      	{
      	  conn=gc.getConnection();	                                  
  		    st = conn.createStatement() ;
  		    rs = st.executeQuery("select student_id from LM_LOAN_ACCOUNT where Loan_Account_No='"+loanid+"'");

  		    while(rs.next())
  		    	studentId=rs.getInt(1);
  		     
      	}
  		    catch (Exception e) 
  	      	{		  
  	      	e.printStackTrace();
  	    	  logger.error(" Exception Occurred in LoanDAO ",e);
  	  		try {
  	  			conn.rollback();
  	  		} catch (SQLException sqle) {
  	  			logger.error(" SQLException while roll back in LoanDAO",e);
  	  		}
  	  	   } finally {
  	  		// Cleanup Resources
  	  		try {
  	  			if(rs != null)
  	  				gc.closeResultSet(rs);
  	  			if(st != null)
  	  				gc.closeStatement(st);
  	  			if(conn != null)
  	  				gc.closeConnection(conn);
  	  		} catch (Exception e) {
  	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
  	  		}
  	      }
  	  	   
  	  	   
  	  	   return studentId;
  	      
      }
      
      /* 
      Parameter : Student ID, date and year
      Return Type : double
      Since: JDK 1.5.0
      See: getConnection(), createStatement(),executeQuery()
    */
      //method to retrieve installment amount on the basis of the student id and month
      public double getInstallmentAmount(int student_id,String date_year)
      {
    	  double principal=0;
    	  double interest=0;
    	  gc=new GenericConnection();
        	try
        	{
        	  conn=gc.getConnection();	                                  
    		    st = conn.createStatement() ;
    		    rs = st.executeQuery("select PRINCIPAL,INTEREST from LM_REPAYMENT_SCHEDULE where student_id="+student_id+" and DEPOSIT_MONTH_YEAR='"+date_year+"'");

    		    while(rs.next())
    		    	{
    		    	  principal=rs.getDouble(1);
    		    	  interest=rs.getDouble(2);
    		    	}
    		     
        	}
    		    catch (Exception e) 
    	      	{		  
    	      	e.printStackTrace();
    	    	  logger.error(" Exception Occurred in LoanDAO ",e);
    	  		try {
    	  			conn.rollback();
    	  		} catch (SQLException sqle) {
    	  			logger.error(" SQLException while roll back in LoanDAO",e);
    	  		}
    	  	   } finally {
    	  		// Cleanup Resources
    	  		try {
    	  			if(rs != null)
    	  				gc.closeResultSet(rs);
    	  			if(st != null)
    	  				gc.closeStatement(st);
    	  			if(conn != null)
    	  				gc.closeConnection(conn);
    	  		} catch (Exception e) {
    	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
    	  		}
    	      }
    	  	   
    	  	   
    	  	   return principal+interest;
      
      }
      
      /* 
      Parameter : Loan Number
      Return Type : void
      Since: JDK 1.5.0
      See: getConnection(), setAutoCommit(),createStatement(),executeQuery()
    */
      //method to update the database after a successful payment
      public void  payInstallment(Loan_Repayment_Details lrd)
      {
        int nextvalue=0;
      	gc=new GenericConnection();
      	try
      	{
      	    Date dt=null;
      		conn=gc.getConnection();	
      	    conn.setAutoCommit(false) ;                                    
  		    st = conn.createStatement() ;
  		    String sql=null;
  		    rs=st.executeQuery("select sysdate from dual");
		    rs.next();
            Date today=rs.getDate(1); 
            rs = st.executeQuery("select repayment_generator.NEXTVAL from dual" ) ;
		    rs.next() ;
		    nextvalue = rs.getInt(1) ;
  		    sql="insert into lm_repayment_details values(?,?,?,?,?)";
  		    pr=conn.prepareStatement(sql);
  		    pr.setInt(1,nextvalue);
  		    pr.setString(2,lrd.getLoan_Account_No());
  		    pr.setDouble(3,lrd.getAmount());
  		    pr.setDate(4,today);
  		    pr.setInt(5,lrd.getFine());
  		     
          	pr.executeUpdate();
    			conn.commit();
             
        	}
        	  catch (Exception e) 
          	{
       		  
          	e.printStackTrace();
        	  logger.error(" Exception Occurred in AccountDAO ",e);
      		try {
      			conn.rollback();
      		} catch (SQLException sqle) {
      			logger.error(" SQLException while roll back in AccountDAO",e);
      		}
      	   } finally {
      		// Cleanup Resources
      		try {
      			if(rs != null)
      				gc.closeResultSet(rs);
      			if(st != null)
      				gc.closeStatement(st);
      			if(conn != null)
      				gc.closeConnection(conn);
      		} catch (Exception e) {
      			logger.error(" Exception while cleaning up resources in AccountDAO",e);
      		}
          }  
  
      }
      
      /* 
      Parameter : object of Loan_Repayment_Details, student_id
      Return Type : void
      Since: JDK 1.5.0
      See: getConnection(), setAutoCommit(), createStatement(),executeQuery()
    */
      //method to deduct installment amount from NSEP account after unsuccessful monthly payment
      
      public void  deductInstallment(Loan_Repayment_Details lrd,int student_id)
      {
        double newBalance=0;
        int nextvalue=0;
      	gc=new GenericConnection();
      	try
      	{
      	    Date dt=null;
      		conn=gc.getConnection();	
      	    conn.setAutoCommit(false) ;                                    
  		    st = conn.createStatement() ;
  		    String sql=null;
  		    rs=st.executeQuery("select sysdate from dual");
		    rs.next();
            Date today=rs.getDate(1); 
            rs = st.executeQuery("select repayment_generator.NEXTVAL from dual" ) ;
		    rs.next() ;
		    nextvalue = rs.getInt(1) ;
  		    sql="insert into  lm_repayment_details values(?,?,?,?,?)";
  		    pr=conn.prepareStatement(sql);
  		    pr.setInt(1,nextvalue);
  		    pr.setString(2,lrd.getLoan_Account_No());
  		    pr.setDouble(3,lrd.getAmount());
  		    pr.setDate(4,today);
  		    pr.setInt(5,lrd.getFine());
  		     
          	pr.executeUpdate();
    			conn.commit();
    			rs = st.executeQuery("select balance from AM_NSEP_Account_Details where Student_Id="+student_id);
    		    rs.next() ;
    		    newBalance = rs.getDouble(1) ;	
    		    newBalance=newBalance-lrd.getAmount();
    		sql="update AM_NSEP_Account_Details set Balance="+newBalance+" where Student_Id="+student_id;
    	    pr=conn.prepareStatement(sql);	
    	    pr.executeUpdate();
			conn.commit();	
             
        	}
        	  catch (Exception e) 
          	{
       		  
          	e.printStackTrace();
        	  logger.error(" Exception Occurred in AccountDAO ",e);
      		try {
      			conn.rollback();
      		} catch (SQLException sqle) {
      			logger.error(" SQLException while roll back in AccountDAO",e);
      		}
      	   } finally {
      		// Cleanup Resources
      		try {
      			if(rs != null)
      				gc.closeResultSet(rs);
      			if(st != null)
      				gc.closeStatement(st);
      			if(conn != null)
      				gc.closeConnection(conn);
      		} catch (Exception e) {
      			logger.error(" Exception while cleaning up resources in AccountDAO",e);
      		}
          }  
  
      }
}

