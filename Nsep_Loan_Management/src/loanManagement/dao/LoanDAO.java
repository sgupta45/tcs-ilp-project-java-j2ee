/*
Author : Anay Arun,Sarfaraz Ahmad
Version : 1.0
Class Name : LoanDao.java
Creation Date : 13/12/2011
History : Initial Creation
*/
package loanManagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import loanManagement.beans.Loan_Repayment_Schedule;
import loanManagement.beans.Loan_Requests;
import loanManagement.util.GenericConnection;

import org.apache.log4j.Logger;

 

public class LoanDAO 
{

	static Logger logger = Logger.getLogger(LoanDAO.class);
	Connection conn = null;
    ResultSet rs = null;
    Statement st=null;
    GenericConnection gc=null;
    PreparedStatement pr=null;
    
    /* 
    Parameter : Request ID
    Return Type : String
    Since: JDK 1.5.0
    See: getConnection(), createStatement(),executeQuery()
  */
    //method to check whether loan request ID provided by user exists in the database or not, returns yes or no 
    public String checkLoanRequestID(String reqid)
    {
    	String result=null;
    	 
    	gc=new GenericConnection();
    	try
    	{
    	  conn=gc.getConnection();	                                  
		    st = conn.createStatement() ;
		    rs = st.executeQuery("select * from LM_Loan_Requests where Loan_Request_No='"+reqid+"'");
		    int i=0;
		    while(rs.next())
		    	i++;
		    if(i==0)
		    	result="no";
		    else
		    	result="yes";
    	}
		    catch (Exception e) 
	      	{		  
	      	e.printStackTrace();
	    	  logger.error(" Exception Occurred in LoanDAO ",e);
	  		try {
	  			conn.rollback();
	  		} catch (SQLException sqle) {
	  			logger.error(" SQLException while roll back in LoanDAO",e);
	  		}
	  	   } finally {
	  		// Cleanup Resources
	  		try {
	  			if(rs != null)
	  				gc.closeResultSet(rs);
	  			if(st != null)
	  				gc.closeStatement(st);
	  			if(conn != null)
	  				gc.closeConnection(conn);
	  		} catch (Exception e) {
	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
	  		}
	      }
	  	   
	  	   
	  	   return result;
	      
    }
    
    /* 
    Parameter : Loan request ID
    Return Type : ArrayList<Loan_Repayment_Schedule>
    Since: JDK 1.5.0
    See: getConnection(), createStatement(),executeQuery()
  */
    
    //returns the schedule of payment/installments in the form of an arraylist of loan_repayment_schedule object
    
    public ArrayList<Loan_Repayment_Schedule> schedule(String reqid)
    { 
    	ArrayList<Loan_Repayment_Schedule> loanSchedule=new ArrayList<Loan_Repayment_Schedule>();
    	gc=new GenericConnection();
    	try
    	{
    	  conn=gc.getConnection();	                               
		    st = conn.createStatement() ;
		    rs = st.executeQuery("select * from LM_Repayment_Schedule where Loan_Request_No='"+reqid+"'");
		    while(rs.next())
		    {
		    
		    	Loan_Repayment_Schedule lrs=new Loan_Repayment_Schedule();
		    	lrs.setLoan_Request_No(rs.getString("Loan_Request_No"));
		    	lrs.setSchedule_ID(rs.getInt("SCHEDULE_ID"));
		    	lrs.setStudent_id(rs.getInt("STUDENT_ID"));
		    	lrs.setPrincipal(rs.getDouble("PRINCIPAL"));
		    	lrs.setInterest(rs.getDouble("INTEREST"));
		    	lrs.setDeposit_month_Year(rs.getString("DEPOSIT_MONTH_YEAR"));
		    	loanSchedule.add(lrs);
		    }
    	}
		    catch (Exception e) 
	      	{		  
	      	e.printStackTrace();
	    	  logger.error(" Exception Occurred in LoanDAO ",e);
	  		try {
	  			conn.rollback();
	  		} catch (SQLException sqle) {
	  			logger.error(" SQLException while roll back in LoanDAO",e);
	  		}
	  	   } finally {
	  		// Cleanup Resources
	  		try {
	  			if(rs != null)
	  				gc.closeResultSet(rs);
	  			if(st != null)
	  				gc.closeStatement(st);
	  			if(conn != null)
	  				gc.closeConnection(conn);
	  		} catch (Exception e) {
	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
	  		}
	      }
	  	   
	  	   
	  	   return loanSchedule;
    }
    
    
    /* 
    Parameter : object of loan_requests
    Return Type : String
    Since: JDK 1.5.0
    See: getConnection(), setAutoCommit(),createStatement(),executeQuery()
  */
    //method for registering new loan requests into the database takes a bean object of loan_request
    public String insertLoanRequestDetails(Loan_Requests lq)
    {
    	String result=null;
    	logger.info("Enter insertLoanRequestDetails()---");	   	
    	gc=new GenericConnection();
    	try
    	{
    	  conn=gc.getConnection();	
    	  conn.setAutoCommit(false) ;                                    
		    st = conn.createStatement() ;
		    rs = st.executeQuery("select request_generator.NEXTVAL from dual" ) ;
		    rs.next() ;
		   
		    int nextvalue = rs.getInt(1) ;
		    System.out.println(nextvalue);
		    String reqid="LNREQ"+nextvalue;
		    String sql=null;
		   
		    if(lq.getRemarks()!= null)
		    {
		    sql="insert into LM_Loan_Requests values(?,?,?,?,?,?,?)";
		    pr=conn.prepareStatement(sql);
		    pr.setString(1, reqid);
		    pr.setInt(2, lq.getStudent_ID());
		    pr.setInt(3, lq.getLoan_Amount());
		    pr.setString(4, lq.getStatus());
		    pr.setString(5,lq.getPurpose_Of_Loan());
		    pr.setString(6, lq.getRemarks());
		    pr.setString(7, lq.getSchedule_generated());

		    }
		    else
		    {
		    	sql="insert into LM_Loan_Requests(Loan_Request_No,Student_ID,Loan_Amount,Status,Purpose_Of_Loan,schedule_generated) values(?,?,?,?,?,?)";
			    pr=conn.prepareStatement(sql);
			    pr.setString(1, reqid);
			    pr.setInt(2, lq.getStudent_ID());
			    pr.setInt(3, lq.getLoan_Amount());
			    pr.setString(4, lq.getStatus());
			    pr.setString(5,lq.getPurpose_Of_Loan());	
			    pr.setString(6,lq.getSchedule_generated());
		    }
		    int i=pr.executeUpdate();
			conn.commit();
			if(i > 0)
			{
				result=reqid;
			}
    	}
    	  catch (Exception e) 
      	{
          result="fail";		  
      	e.printStackTrace();
    	  logger.error(" Exception Occurred in LoanDAO ",e);
  		try {
  			conn.rollback();
  		} catch (SQLException sqle) {
  			logger.error(" SQLException while roll back in LoanDAO",e);
  		}
  	   } finally {
  		// Cleanup Resources
  		try {
  			if(rs != null)
  				gc.closeResultSet(rs);
  			if(st != null)
  				gc.closeStatement(st);
  			if(conn != null)
  				gc.closeConnection(conn);
  		} catch (Exception e) {
  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
  		}
      }
  	   
  	   logger.info("Exit insertLoanRequestDetails()---");	
  	   return result;
      }
    
    /* 
    Parameter : Request ID
    Return Type : object of Loan_Requests
    Since: JDK 1.5.0
    See: getConnection(), createStatement(),executeQuery()
  */
    // method for getting Loan Amount issued and the student ID associated with the loan Amount, takes loan request ID as input  
    public Loan_Requests getLoanAmountandStudentID(String reqid)
    {
    	int loan_amount=0,student_id=0;Loan_Requests lr=null;
    	gc=new GenericConnection();
    	try
    	{
    	  conn=gc.getConnection();	                                   
		    st = conn.createStatement() ;
		    rs = st.executeQuery("select Loan_Amount,Student_ID from LM_Loan_Requests where Loan_Request_No='"+reqid+"'");
		    rs.next();
		    	 
		    	loan_amount=rs.getInt("Loan_Amount");
		    	student_id=rs.getInt("Student_ID"); 
		    
		    lr=new Loan_Requests();
		    lr.setStudent_ID(student_id);
		    lr.setLoan_Amount(loan_amount);
    	}
		    catch (Exception e) 
	      	{		  
	      	e.printStackTrace();
	    	  logger.error(" Exception Occurred in LoanDAO ",e);
	  		try {
	  			conn.rollback();
	  		} catch (SQLException sqle) {
	  			logger.error(" SQLException while roll back in LoanDAO",e);
	  		}
	  	   } finally {
	  		// Cleanup Resources
	  		try {
	  			if(rs != null)
	  				gc.closeResultSet(rs);
	  			if(st != null)
	  				gc.closeStatement(st);
	  			if(conn != null)
	  				gc.closeConnection(conn);
	  		} catch (Exception e) {
	  			logger.error(" Exception while cleaning up resources in LoanDAO",e);
	  		}
	      }
	  	   
     	   
	  	   return lr;
    
    }

    
    /* 
    Parameter :ArrayList<Loan_Repayment_Schedule> schedules
    Return Type : void
    Since: JDK 1.5.0
    See: getConnection(), setAutoCommit(),createStatement(),executeQuery()
  */
    // method generates the loan repayment schedule, takes a bean object of type Loan_Repayment_Schedule as parameter
    public void generateScedule(ArrayList<Loan_Repayment_Schedule> schedules)
    {
    	gc=new GenericConnection();
    	try
    	{
    		String lnreqno=null;
    	    conn=gc.getConnection();	
    	    conn.setAutoCommit(false) ;                                    
		    st = conn.createStatement() ;
		    String sql=null;
		    
          for(Loan_Repayment_Schedule ln : schedules)
          {
        	rs = st.executeQuery("select schedule_generator.NEXTVAL from dual" ) ;
  		    rs.next() ;
  		    int nextvalue = rs.getInt(1) ;
  		    sql="insert into LM_Repayment_Schedule values(?,?,?,?,?,?)";
		    pr=conn.prepareStatement(sql);
		    pr.setInt(1,nextvalue);
		    pr.setInt(2, ln.getStudent_id());
		    pr.setString(3, ln.getLoan_Request_No());
		    pr.setDouble(4, ln.getPrincipal());
		    pr.setDouble(5,ln.getInterest());
		    pr.setString(6, ln.getDeposit_month_Year());
        	pr.executeUpdate();
  			conn.commit();
  			lnreqno=ln.getLoan_Request_No();
          }
          sql="update LM_Loan_Requests set schedule_generated ='yes' where LOAN_REQUEST_NO='"+lnreqno+"'";
        	st.executeUpdate(sql);
  			conn.commit();
      	}
      	  catch (Exception e) 
        	{
     		  
        	e.printStackTrace();
      	  logger.error(" Exception Occurred in LoanDAO ",e);
    		try {
    			conn.rollback();
    		} catch (SQLException sqle) {
    			logger.error(" SQLException while roll back in LoanDAO",e);
    		}
    	   } finally {
    		// Cleanup Resources
    		try {
    			if(rs != null)
    				gc.closeResultSet(rs);
    			if(st != null)
    				gc.closeStatement(st);
    			if(conn != null)
    				gc.closeConnection(conn);
    		} catch (Exception e) {
    			logger.error(" Exception while cleaning up resources in LoanDAO",e);
    		}
        }  
          }
    }
  


