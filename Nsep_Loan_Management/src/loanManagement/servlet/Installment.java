/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : Installment.java
Creation Date : 14/12/2011
History : Initial Creation
*/





package loanManagement.servlet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.dao.AccountDAO;

/**
 * Servlet implementation class Installment
 */
public class Installment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Installment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("button").equals("ok"))
		{
			response.sendRedirect("./jsp/loan_id.jsp");
		}
		else
		{
		AccountDAO ad=new AccountDAO();
		int fine=0;
		int studid=ad.getStudentID(request.getParameter("hidden"));
		String months[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
		 Date dt=new Date();
    	 int month=dt.getMonth();
    	 int year=dt.getYear();
    	 year=year+1900;
    	 double amount=ad.getInstallmentAmount(studid,months[month]+","+year);
    	 if(dt.getDate() > 20 && dt.getDate() <=25 )
    		 fine=(dt.getDate()-20)*10;
    	 if(dt.getDate() > 25)
    		 fine=50;
    	 DecimalFormat twoDForm = new DecimalFormat("#.##");
    	 amount=Double.valueOf(twoDForm.format(amount));
    	 request.setAttribute("amount", amount);
    	 request.setAttribute("fine", fine);
    	 request.setAttribute("loanid",request.getParameter("hidden"));
    	 request.setAttribute("student_id",studid);
    	 RequestDispatcher rd=request.getRequestDispatcher("/jsp/showInstallment.jsp");
    	 rd.forward(request, response);
		}
	}

}
