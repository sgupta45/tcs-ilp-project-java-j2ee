/*
Authors : Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : PayDeductInstallment.java
Creation Date : 14/12/2011
History : Initial Creation
*/


package loanManagement.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Repayment_Details;
import loanManagement.dao.AccountDAO;

/**
 * Servlet implementation class PayDeductInstallment
 */
public class PayDeductInstallment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayDeductInstallment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	  AccountDAO ad=new AccountDAO();
	  Loan_Repayment_Details lrd=new Loan_Repayment_Details();
	  Double amt=new Double(request.getParameter("amount"));
	  lrd.setLoan_Account_No(request.getParameter("loanid"));
	  lrd.setAmount(amt);
	  lrd.setFine(Integer.parseInt(request.getParameter("fine")));
	  if(request.getParameter("installment").equals("Pay"))
		  {
		   ad.payInstallment(lrd);
		   request.setAttribute("message", "Installment paid successfully");
		  }
	  else
		  {
		   ad.deductInstallment(lrd,Integer.parseInt(request.getParameter("student_id")));
		   request.setAttribute("message", "Installment deducted successfully from NSEP Account");
		  }
	  RequestDispatcher rd=request.getRequestDispatcher("/jsp/showPaymentMessage.jsp");
	  rd.forward(request, response);
	}

}
