/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : PendingRequests.java
Creation Date : 13/12/2011
History : Initial Creation
*/






package loanManagement.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Requests;
import loanManagement.dao.AccountDAO;

/**
 * Servlet implementation class PendingRequests
 */
public class PendingRequests extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PendingRequests() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	    ArrayList<Loan_Requests> requests=new ArrayList<Loan_Requests>();
		AccountDAO ad=new AccountDAO();	
		requests=ad.requestDetails();
		request.setAttribute("getrequests", requests);
		RequestDispatcher rd=request.getRequestDispatcher("/jsp/viewPendingRequests.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
