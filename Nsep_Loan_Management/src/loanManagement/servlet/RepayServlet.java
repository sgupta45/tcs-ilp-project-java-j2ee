/*
Authors : Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : RepayServlet.java
Creation Date : 14/12/2011
History : Initial Creation
*/



package loanManagement.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Repayment_Details;
import loanManagement.dao.AccountDAO;

/**
 * Servlet implementation class RepayServlet
 */
public class RepayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RepayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	  ArrayList<Loan_Repayment_Details> history=new ArrayList<Loan_Repayment_Details>();	
      AccountDAO ad=new AccountDAO();
      String present=ad.checkLoanID(request.getParameter("loanid"));
      request.setAttribute("present", present);
      if(present.equals("yes"))
      {
      history=ad.repaymentDetails(request.getParameter("loanid"));
      request.setAttribute("history", history);
      String allpaid=ad.ifAllInstallmentsPaid(request.getParameter("loanid"));
      request.setAttribute("allpaid", allpaid);
      request.setAttribute("loannumber",request.getParameter("loanid"));
      String currentStatus=ad.currentMonthRepaymentStatus(request.getParameter("loanid"));
      request.setAttribute("currentStatus", currentStatus);
      }
      RequestDispatcher rd=request.getRequestDispatcher("/jsp/repayDetails.jsp");
      rd.forward(request, response);
	}
	
	

}
