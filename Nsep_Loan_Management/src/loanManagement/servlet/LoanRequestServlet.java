/*
Author : Anay Arun,Sarfaraz Ahmad
Version : 1.0
Class Name : LoanRequestServlet.java
Creation Date : 13/12/2011
History : Initial Creation
*/
package loanManagement.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Requests;
import loanManagement.dao.LoanDAO;

/**
 * Servlet implementation class LoanRequestServlet
 */
public class LoanRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoanRequestServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  LoanDAO ld=new LoanDAO();	
		  Loan_Requests lr=new Loan_Requests();
		  lr.setStudent_ID(Integer.parseInt(request.getParameter("studid")));
		  lr.setLoan_Amount( Integer.parseInt(request.getParameter("lamnt")));
		  String []purpose=request.getParameterValues("purpose");
		   
		  if(request.getParameter("other") != null)
			  purpose[0]=request.getParameter("other");
		  lr.setPurpose_Of_Loan(purpose[0]);
		  if(request.getParameter("remark") != null)
			  lr.setRemarks(request.getParameter("remark"));
		  lr.setStatus("pending");
		  lr.setSchedule_generated("no");
		  String result=ld.insertLoanRequestDetails(lr);
		  if(result.equals("fail"))
		  {
			  response.sendRedirect("./jsp/error_request.jsp");
		  }
		  else
		  {
			 RequestDispatcher rd=request.getRequestDispatcher("/jsp/generate_loanRequest.jsp");
			 request.setAttribute("request_id",result);
			 rd.forward(request, response);
			 
		  }
	}

}
