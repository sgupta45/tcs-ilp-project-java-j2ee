/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : ScheduleServlet.java
Creation Date : 13/12/2011
History : Initial Creation
*/





package loanManagement.servlet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Repayment_Schedule;
import loanManagement.beans.Loan_Requests;
import loanManagement.dao.LoanDAO;

/**
 * Servlet implementation class ScheduleServlet
 */
public class ScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScheduleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoanDAO ld=new LoanDAO();
		ArrayList<Loan_Repayment_Schedule> loanSchedule=new ArrayList<Loan_Repayment_Schedule>();
		String months[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
		if(ld.checkLoanRequestID(request.getParameter("reqid")).equals("yes"))
		{
		     loanSchedule=ld.schedule(request.getParameter("reqid"));
		     if(loanSchedule.isEmpty())
		     {
		    	 Loan_Requests lr=null;
		    	 Date dt=new Date();
		    	 int month=dt.getMonth();
		    	 int year=dt.getYear();
		    	 year=year+1900;
		    	 lr=ld.getLoanAmountandStudentID(request.getParameter("reqid"));
		    	 Random r=new Random();
		    	 int x=r.nextInt(10)+3;
		    	 int quotient=lr.getLoan_Amount()/x;
		    	 int remainder=lr.getLoan_Amount()%x;
		    	 DecimalFormat twoDForm = new DecimalFormat("#.##");
		    	 for(int i=1;i<=x;i++)
		    	 {
		    		 month++;
		    		 if(month > 11)
		    		 {
		    			 month=0;
		    			 year++;
		    		 }
		    		 if(i==x)
		    			 quotient=quotient+remainder;
		    		 Loan_Repayment_Schedule lrs=new Loan_Repayment_Schedule();
		    		 lrs.setStudent_id(lr.getStudent_ID());
		    		 lrs.setLoan_Request_No(request.getParameter("reqid"));
		    		 lrs.setPrincipal(quotient);
		    		 double interest=quotient*0.02;
		    		 lrs.setInterest(Double.valueOf(twoDForm.format(interest)));
		    		 lrs.setDeposit_month_Year(months[month]+","+year);
		    		 loanSchedule.add(lrs);
		    	 }
		    	 ld.generateScedule(loanSchedule);
		     }
		    	 RequestDispatcher rd=request.getRequestDispatcher("/jsp/viewSchedule.jsp");
		    	 request.setAttribute("schedules", loanSchedule);
		    	 rd.forward(request, response);
		
		}
		else
		{
			 response.sendRedirect("./jsp/error_norequestID.jsp");
		}
	}

}
