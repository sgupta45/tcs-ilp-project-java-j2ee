/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : PendingRequests.java
Creation Date : 13/12/2011
History : Initial Creation
*/





package loanManagement.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loanManagement.beans.Loan_Account;
import loanManagement.beans.Loan_Requests;
import loanManagement.dao.AccountDAO;
import loanManagement.dao.LoanDAO;

/**
 * Servlet implementation class AcceptReject
 */
public class AcceptReject extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcceptReject() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		AccountDAO ad=new AccountDAO();
		String str=request.getParameter("hidden");
		String []arr=str.split(" ");
		Loan_Requests lr=new Loan_Requests();
		Loan_Account la=new Loan_Account();
		LoanDAO ld=new LoanDAO();
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		if(arr[0].equals("Accept"))
		{
		  	ad.updateRequestStatus(arr[1]);
		  	lr=ld.getLoanAmountandStudentID(arr[1]);
		  	la.setLoan_Request_No(arr[1]);
		  	la.setLoan_Amount(lr.getLoan_Amount());
		  	la.setStudent_ID(lr.getStudent_ID());
		  	String loannumber=ad.createLoanAccount(la);
		  	request.setAttribute("loannumber", loannumber);
		  	RequestDispatcher rd=request.getRequestDispatcher("/jsp/generate_Loan_Number.jsp");
		  	rd.forward(request, response);
		}
		else
		{
		  ad.deleteRequest(arr[1]);	
		  RequestDispatcher rd=request.getRequestDispatcher("/jsp/reject_request.jsp");
		  rd.forward(request, response);
		}

		
	}

}
