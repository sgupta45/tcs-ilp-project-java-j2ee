/*
Authors : Apurva Thakur, Prashant Pandey, Anay Arun, Sarfaraz Ahmad, Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : Generic Connection.java
Creation Date : 15/12/2011
History : Initial Creation
*/





package loanManagement.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import loanManagement.beans.Loan_Repayment_Schedule;

import org.apache.log4j.Logger;

public class GenericConnection 
{
	static Logger logger = Logger.getLogger(GenericConnection.class);
	Connection conn = null;
    ResultSet rs = null;
    Statement st=null;
    
    /* 
    Parameter : No parameters
    Return Type : Connection object
    Exception: Throws Exception class object
    Since: JDK 1.5.0
    
  */
    
	public Connection getConnection() throws Exception
	{
	
	logger.info("Enter getConnection()---");	
	try {
 		// Load Driver and Register with Driver Manager
		Class.forName("oracle.jdbc.driver.OracleDriver");

		String dburl = "jdbc:oracle:thin:@INGNRILPINFM01:1521:ORCL";
		String dbusername = "a50c";
		String dbpassword = "a50c";

		// Get the Connection object from Driver Manager
		conn = DriverManager.getConnection(dburl,dbusername, dbpassword);

    	}
	catch (Exception e) 
	{ 
			logger.error(" Exception Occurred while getting the connection",e);
			throw e;
    }
	logger.info("Exit getConnection()---");
	return conn;
	}
	
	/* 
    Parameter :Connection object
    Return Type : void
    Exception: Throws Exception class object
    Since: JDK 1.5.0
    
  */
	
	public void closeConnection(Connection con)throws Exception
	{
		logger.info("Enter closeConnection()---");
		try {
		     con.close();
		}
		catch (Exception e) 
		{ 
				logger.error(" Exception Occurred while closing the Connection",e);
				throw e;
	    }
		logger.info("Exit closeConnection()---");
	}	
	
	/* 
    Parameter :Statement object
    Return Type : void
    Exception: Throws Exception class object
    Since: JDK 1.5.0
    
  */
	
	public void closeStatement(Statement st)throws Exception
	{
		logger.info("Enter closeStatement()---");
		try {
		     st.close();
		}
		catch (Exception e) 
		{ 
				logger.error(" Exception Occurred while closing the statement/preparedStatement",e);
				throw e;
	    }
		logger.info("Exit closeStatement()---");
	}
	
	/* 
    Parameter :ResultSet object
    Return Type : void
    Exception: Throws Exception class object
    Since: JDK 1.5.0
    
  */
	public void closeResultSet(ResultSet rs)throws Exception
	{
		logger.info("Enter closeResultSet()---");
		try {
		     rs.close();
		}
		catch (Exception e) 
		{ 
				logger.error(" Exception Occurred while closing the ResultSet",e);
				throw e;
	    }
		logger.info("Exit closeResultSet()---");
	}		  
}


