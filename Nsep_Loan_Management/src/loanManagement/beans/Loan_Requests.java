/*
Authors : Anay Arun,Sarfaraz Ahmad
Version : 1.0
Class Name : Loan_Requests.java
Creation Date : 12/12/2011
History : Initial Creation
*/
package loanManagement.beans;

public class Loan_Requests {

	private String loan_Request_No;
	private int student_ID;
	private int loan_Amount;
	private String status;
	private String purpose_Of_Loan;
	private String remarks;
	private String schedule_generated ;
	
	// Generate Getters and Setters
	
	public String getLoan_Request_No() {
		return loan_Request_No;
	}
	public void setLoan_Request_No(String loanRequestNo) {
		loan_Request_No = loanRequestNo;
	}
	public int getStudent_ID() {
		return student_ID;
	}
	public void setStudent_ID(int studentID) {
		student_ID = studentID;
	}
	public int getLoan_Amount() {
		return loan_Amount;
	}
	public void setLoan_Amount(int loanAmount) {
		loan_Amount = loanAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPurpose_Of_Loan() {
		return purpose_Of_Loan;
	}
	public void setPurpose_Of_Loan(String purposeOfLoan) {
		purpose_Of_Loan = purposeOfLoan;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getSchedule_generated() {
		return schedule_generated;
	}
	public void setSchedule_generated(String scheduleGenerated) {
		schedule_generated = scheduleGenerated;
	}
 	 
}
