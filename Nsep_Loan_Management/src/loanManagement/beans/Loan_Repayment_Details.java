/*
Authors : Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : Loan_Repayment_Details.java
Creation Date : 12/12/2011
History : Initial Creation
*/



package loanManagement.beans;

import java.sql.Date;

public class Loan_Repayment_Details {
  private int repayment_ID;
  private String loan_Account_No;
  private double amount;
  private Date deposit_Date;
  private int fine;
  
//Generate Getters and Setters
  
public int getRepayment_ID() {
	return repayment_ID;
}
public void setRepayment_ID(int repaymentID) {
	repayment_ID = repaymentID;
}
public String getLoan_Account_No() {
	return loan_Account_No;
}
public void setLoan_Account_No(String loanAccountNo) {
	loan_Account_No = loanAccountNo;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public Date getDeposit_Date() {
	return deposit_Date;
}
public void setDeposit_Date(Date depositDate) {
	deposit_Date = depositDate;
}
public int getFine() {
	return fine;
}
public void setFine(int fine) {
	this.fine = fine;
}
 

  
}
