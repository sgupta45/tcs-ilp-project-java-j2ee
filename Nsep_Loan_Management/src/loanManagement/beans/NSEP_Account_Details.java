/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : NSEP_Account_Details.java
Creation Date : 12/12/2011
History : Initial Creation
*/




package loanManagement.beans;

public class NSEP_Account_Details {
	
	private int student_Id;
	private int nsep_Account_Number;
	private String bank_Name;
	private double balance;
	
	// Generate Getters and Setters
	public int getStudent_Id() {
		return student_Id;
	}
	public void setStudent_Id(int student_Id) {
		this.student_Id = student_Id;
	}
	public int getNsep_Account_Number() {
		return nsep_Account_Number;
	}
	public void setNsep_Account_Number(int nsep_Account_Number) {
		this.nsep_Account_Number = nsep_Account_Number;
	}
	public String getBank_Name() {
		return bank_Name;
	}
	public void setBank_Name(String bank_Name) {
		this.bank_Name = bank_Name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	 
}
