/*
Authors : Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : Student_Account_Details.java
Creation Date : 12/12/2011
History : Initial Creation
*/




package loanManagement.beans;

import java.sql.Date;

public class Student_Account_Details {
	
	private int student_Id;
	private int student_Account_Number;
	private String bank_Name;
	private String type_of_account;
	private int committed_Amount;
	private int maturity_Period;
	private Date maturity_Date;
	private Date account_Creation_Date;
	private double balance;
	
	// Generate Getters and Setters
	public int getStudent_Id() {
		return student_Id;
	}
	public void setStudent_Id(int student_Id) {
		this.student_Id = student_Id;
	}
	public int getStudent_Account_Number() {
		return student_Account_Number;
	}
	public void setStudent_Account_Number(int student_Account_Number) {
		this.student_Account_Number = student_Account_Number;
	}
	public String getBank_Name() {
		return bank_Name;
	}
	public void setBank_Name(String bank_Name) {
		this.bank_Name = bank_Name;
	}
	public String getType_of_account() {
		return type_of_account;
	}
	public void setType_of_account(String type_of_account) {
		this.type_of_account = type_of_account;
	}
	public int getCommitted_Amount() {
		return committed_Amount;
	}
	public void setCommitted_Amount(int committed_Amount) {
		this.committed_Amount = committed_Amount;
	}
	public int getMaturity_Period() {
		return maturity_Period;
	}
	public void setMaturity_Period(int maturity_Period) {
		this.maturity_Period = maturity_Period;
	}
	public Date getMaturity_Date() {
		return maturity_Date;
	}
	public void setMaturity_Date(Date maturity_Date) {
		this.maturity_Date = maturity_Date;
	}
	public Date getAccount_Creation_Date() {
		return account_Creation_Date;
	}
	public void setAccount_Creation_Date(Date account_Creation_Date) {
		this.account_Creation_Date = account_Creation_Date;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	 
}
