/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : Student_Monthly_Contribution.java
Creation Date : 12/12/2011
History : Initial Creation
*/




package loanManagement.beans;

import java.sql.Date;

public class Student_Monthly_Contribution {
	
	private int student_Id;
	private Date date_of_deposit;
	private int deposited_Amount;
	
	// Generate Getters and Setters
	public int getStudent_Id() {
		return student_Id;
		
	}
	public void setStudent_Id(int student_Id) {
		this.student_Id = student_Id;
	}
	public Date getDate_of_deposit() {
		return date_of_deposit;
	}
	public void setDate_of_deposit(Date date_of_deposit) {
		this.date_of_deposit = date_of_deposit;
	}
	public int getDeposited_Amount() {
		return deposited_Amount;
	}
	public void setDeposited_Amount(int deposited_Amount) {
		this.deposited_Amount = deposited_Amount;
	}
	 
}
