/*
Authors : Anshul Gupta, Shruti Gupta
Version : 1.0
Class Name : Loan_Repayment_Schedule.java
Creation Date : 12/12/2011
History : Initial Creation
*/



package loanManagement.beans;

import java.sql.Date;

public class Loan_Repayment_Schedule {
	
	private int schedule_ID;
	private int student_id;
	private String loan_Request_No;
	private double principal;
	private double interest;
	private String deposit_month_Year;
	
	// Generate Getters and Setters
	
	public int getSchedule_ID() {
		return schedule_ID;
	}
	public void setSchedule_ID(int schedule_ID) {
		this.schedule_ID = schedule_ID;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public String getLoan_Request_No() {
		return loan_Request_No;
	}
	public void setLoan_Request_No(String loan_Request_No) {
		this.loan_Request_No = loan_Request_No;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public String getDeposit_month_Year() {
		return deposit_month_Year;
	}
	public void setDeposit_month_Year(String deposit_month_Year) {
		this.deposit_month_Year = deposit_month_Year;
	}
	 
	 
}
