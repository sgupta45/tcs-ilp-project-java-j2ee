/*
Authors : Apurva Thakur, Prashant Pandey
Version : 1.0
Class Name : Loan_Account.java
Creation Date : 12/12/2011
History : Initial Creation
*/

package loanManagement.beans;

import java.sql.Date;

public class Loan_Account {

	private String loan_Account_No;
	private int student_ID;
	private String loan_Request_No; 
	private int loan_Amount;
	private Date start_date;
	
	// Generate Getters and Setters
	
	public String getLoan_Account_No() {
		return loan_Account_No;
	}
	public void setLoan_Account_No(String loan_Account_No) {
		this.loan_Account_No = loan_Account_No;
	}
	public int getStudent_ID() {
		return student_ID;
	}
	public void setStudent_ID(int student_ID) {
		this.student_ID = student_ID;
	}
	public String getLoan_Request_No() {
		return loan_Request_No;
	}
	public void setLoan_Request_No(String loan_Request_No) {
		this.loan_Request_No = loan_Request_No;
	}
	public int getLoan_Amount() {
		return loan_Amount;
	}
	public void setLoan_Amount(int loan_Amount) {
		this.loan_Amount = loan_Amount;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	 
	
}
