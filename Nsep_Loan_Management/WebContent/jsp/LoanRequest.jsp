<%@page import="loanManagement.beans.Loan_Requests"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" /> 
  <script type="text/javascript" src="validations.js">
 	  </script>
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="../PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	
	<div id="main" style="HEIGHT: 620px;">
	<div style=" POSITION: absolute; TOP: 300px;left:350px;">
		<form name="loanRequestForm" style="top:320px;" action="../LoanRequestServlet" method="post" onsubmit="return check()" onreset="checkReset()">
	<center>
	<table width="600" border="1" >
 
    <tr>
    <td  colspan="2">
    <h2><font color="brown">  Loan Registration</font></h2>
    </td>
    </tr>
	<tr>
		<td width="250" style="margin-left:20px;font-size:15px;font-family:Times New Roman;text-align:left;">Student ID</td>
		<td style="text-align:left"><input type="text" name="studid" /><font color="red">*</font></td>
	</tr>
	<tr>
		<td style="text-align:left;">Loan Amount</td>
		<td style="text-align:left;"><input type="text" name="lamnt" /><font color="red">*</font></td>
	</tr>
	<tr>
<td style="text-align:left;">
Purpose
</td>
<td style="text-align:left;">
<select style="width:300px;" name="purpose" id="purpose" onchange="loanPurpose()">
<option value="---Select Purpose---">---Select Purpose---</option>
<option value="Exam fee of a Competitive Exam">Exam fee of a Competitive Exam</option>
 <option value="Purchase of Academic Books">Purchase of Academic Books</option>
 <option value="To enroll in any technical certification course">To enroll in any technical certification course</option>
 <option value="Others">Others</option>
</select><font color="red">*</font></td>
</tr>
	<tr>
		<td style="text-align:left;">Other Purpose (please specify)</td>
		<td style="text-align:left;"><input type="text" style="background-color:#C0C0C0;" name="other" disabled="disabled"  /></td>
	</tr> 
<tr>
		<td style="text-align:left;">Remarks</td>
		<td style="text-align:left;"><input type="text" name="remark" /></td>
	</tr>
</table>
<br><br>
<input type="submit" value="Submit" />&nbsp&nbsp&nbsp&nbsp&nbsp
<input type="reset" value="Reset" />
</center>
 </form>
	</div>
	</div>
 

	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>