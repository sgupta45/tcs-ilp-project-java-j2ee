<%@page import="java.io.PrintWriter"%>
<%@page import="loanManagement.beans.Loan_Requests"%>
<%@page import="loanManagement.beans.Loan_Repayment_Schedule"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="./jsp/style.css" />
<script type="text/javascript">
function fun(a,b)
{
	

	x=document.getElementsByName("hidden");
	x[0].value=	a+" "+b;
	 
    
}
</script>
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="./jsp/logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="./jsp/LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="./jsp/GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="./PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="./jsp/loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="./jsp/terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	<div id="main" style="width:720px;height:620px;">
	<div style=" POSITION: absolute; TOP: 200px;left:340px;">
	<center>
		<form name="pendingrequestView" style="top:320px;" action="./AcceptReject" method="post">
		<%
		 ArrayList<Loan_Requests> requests=(ArrayList<Loan_Requests>) request.getAttribute("getrequests");
		 if(requests.isEmpty())
		 {
			%> 
			 
			 
		 <b>No pending request(s) !!</b>
		  <% 
		 }
		 else
		 {
		%>
		<table  border="1" style="width:650px">
		<tr>
		<td>
		<b>Loan Request Number</b> 
		</td>
		<td>
		<b>Student ID</b>
		</td>
		<td>
		<b>Loan Amount</b>
		</td>
		<td>
		<center>
		<b>Purpose</b>
		</center>
		</td>
		<td colspan="2">
		<center>
		<b>Action</b>
		</center>
		</td>
		</tr>
		<%
		    for(Loan_Requests getrequest: requests)
		    {
		%>
		<tr>
		    <td>
		    <%=getrequest.getLoan_Request_No()%>
		    </td>
		    <td>
		    <%=getrequest.getStudent_ID()%>
		    </td>
		    <td>
		    <%=getrequest.getLoan_Amount()%>
		    </td>
		    <td>
		    <%=getrequest.getPurpose_Of_Loan() %>
		    </td>
		<td>
	    <input type="hidden" name="hidden" /> 
		<button style="width:80px" name="button" type="submit" value="Accept" onclick="fun('Accept','<%=getrequest.getLoan_Request_No() %>')">Accept</button>
		</td>
		<td>
		<button style="width:80px" name="button" type="submit" value="Reject" onclick="fun('Reject','<%=getrequest.getLoan_Request_No() %>')">Reject</button>
        </td>
		</tr>
		<%
		} 
		}%>
		</table>
	    </form>
	</center>
	</div>

</div>
	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
