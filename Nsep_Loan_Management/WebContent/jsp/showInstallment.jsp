<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.text.Format"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="./jsp/style.css" />
<script type="text/javascript" src="../jsp/validations.js">
</script>
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="./jsp/logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="./jsp/LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="./jsp/GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="./PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="./jsp/loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="./jsp/terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	<div id="main" style="height:620px">
	<div style=" POSITION: absolute; TOP: 300px;left:490px;">
	<center>
		<form name="showInstallments" style="top:320px;" action="./PayDeductInstallment" method="post">
		<input type="hidden" name="amount" value="<%=request.getAttribute("amount") %>"/>
		<input type="hidden" name="loanid" value="<%=request.getAttribute("loanid") %>"/>
		<input type="hidden" name="fine" value="<%=request.getAttribute("fine") %>"/>
		<input type="hidden" name="student_id" value="<%=request.getAttribute("student_id") %>"/>
		 
		<table border="1">
		<% Format formatter = new SimpleDateFormat("dd-MMM-yyyy");  %>
		<tr >
		<td colspan="3" >
		<center>
		<font color="brown">
		<b>
		Installment Details for Current Month</b>
		</font>
		</center>
		</td>
		</tr>
		<tr>
		<td>
		<b>Date</b>
		</td>
		<td>
		<b>Installment Amount</b>
		</td>
		<td>
		<b>Fine</b>
		</td>
		</tr>
		<tr>
		<td>
		<%= formatter.format(new Date()) %>
		</td>
		
		<td>
		<%= request.getAttribute("amount") %>
		</td>
		
		<td>
		<%= request.getAttribute("fine") %>
		</td>
		</tr>
		<tr >
		<td colspan="3">
		<center>
		<% if(request.getParameter("button").equals("Pay installment for current month"))
		{
			%>
		<input style="width:80px"  type="submit" name="installment" value="Pay" />
		<%
		}
		else
		{
		%>
		<input style="width:80px"  type="submit" name="installment" value="Deduct" />
		<%
		}
		%>
		</center>
		</td>
		</tr>
		
		</table>
	
		
	    </form>
	</center>
	</div>

</div>
	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
