<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../jsp/style.css" />
<script type="text/javascript" src="../jsp/validations.js">
</script>
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="../jsp/logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="../jsp/home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="../jsp/GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="../PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	<div id="main" style="height:620px">
	<div style=" POSITION: absolute; TOP: 300px;left:550px;">
	<center>
		<form name="error" style="top:320px;">
		<table border="1">
		<tr>
		<td>
		This request cannot be served !!
		</td>
		</tr >
		<tr>
		<td>
		<center>
		<a href="LoanRequest.jsp">Ok</a>
		</center>
		</td>
		</tr>
		
		</table>
	
		
	    </form>
	</center>
	</div>

</div>
	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
