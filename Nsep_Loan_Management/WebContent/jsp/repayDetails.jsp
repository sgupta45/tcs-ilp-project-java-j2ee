<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.Date"%>
<%@page import="loanManagement.beans.Loan_Repayment_Details"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="./jsp/style.css" />
<script type="text/javascript" src="./jsp/validations.js">
 
</script>
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="./jsp/logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="./jsp/LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="./jsp/GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="./PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="./jsp/loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="./jsp/terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	
	<div id="main" style="width:720px;height:620px;">
	<div style=" POSITION: absolute; TOP: 250px;left:480px;">
	<center>
	
		<form name="repayHistoryForm" style="top:400px;" action="./Installment" method="post">
		
		<table border="1" width="420px">
		<%
	   String present=(String)request.getAttribute("present");
	   ArrayList <Loan_Repayment_Details> history=(ArrayList <Loan_Repayment_Details>) request.getAttribute("history");
	   Format formatter = new SimpleDateFormat("dd-MMM-yyyy"); 
	   if(present.equals("no"))
	   {
	%>
	   
	   <tr><td colspan="3" ><center>Invalid LoanID</center></td></tr>
	   
	   <tr><td colspan="3" ><center><a href="./jsp/loan_id.jsp">Ok</a></center></td></tr>
	   <%
	   }
	   else
	   {
		   
		 %>
		 <input type="hidden" name="hidden" value="<%=request.getAttribute("loannumber") %>"/>
		 <tr>
		<td colspan="3">
		<center><b>Loan ID:<%=request.getAttribute("loannumber")%></b></center>
		</td>
		</tr>
		 <%   
		   if(history.isEmpty())
		   {
	   %>
	   <tr><td colspan="3"><center>No previous repayment history</center></td></tr>
	   <%
		   }
		   else
		   {
	   %>
		<tr>
		<td colspan="3"><center><b><font color="brown">
		Previous Installments </font>
		</b></center>
		</td>
		</tr>
				<tr>
		<td><center><b>
		Installment Amount
		</b></center>
		</td>
		<td><center><b>
		Date of Deposit
		</b></center>
		</td>
		<td><center><b>
		Fine
		</b></center>
		</td>
		</tr>
		<%
		  for(Loan_Repayment_Details details :history)
		  {
		%>
		<tr >
		<td>
		<center><%= details.getAmount() %></center>
		</td>
		<td>
		<%= formatter.format(details.getDeposit_Date()) %>
		</td>
		<td>
		<%= details.getFine() %>
		</td>
		</tr>
		<%
		  }
		   }
		   if(request.getAttribute("allpaid").equals("yes"))
		   {
	    %>
	         
	      
	         <tr> <td colspan="3"><center>Notice : All installments are paid.</center></td></tr>
	         
	       <tr> <td colspan="3"><center><input style="width:80px;" type="submit" name="button" value="ok" /></center></td></tr>  
	    <% 		   
		   }
		   else
		   {
			   if(request.getAttribute("currentStatus").equals("paid"))
			   {
			   %>
		<tr> <td colspan="3"><center>Notice : Installment for the current month is paid.</center></td></tr>
         
       <tr> <td colspan="3"><center><input style="width:80px;" type="submit" name="button" value="ok" ></center></td></tr>
       <% 
			   }
			   else
			   {
			   Date d=new Date();
			   if(d.getDate() > 25)
			   {
				   %>
			
	        <tr> <td colspan="3"><center>Notice : Deadline for current installment is over.</center></td></tr>
	
	      <tr> <td colspan="3"><center> 
	      <button style="width:330px;" type="submit" name="button" value="Deduct installment of current month from NSEP account" >Deduct installment of current month from NSEP account</button></center></td></tr>     
				   
				   <%
			   }
			   else
			   {
				  
	    	   %>
	       <tr> <td colspan="3"><center>
	       <button type="submit" name="button" value="Pay installment for current month" > Pay installment for current month</button></center></td></tr>     
				   
				   <% 
	       }
			   }
		   }
	   }
		%>
		</table>
	    
		
	    </form>
	</center>
	</div>

</div>
	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
