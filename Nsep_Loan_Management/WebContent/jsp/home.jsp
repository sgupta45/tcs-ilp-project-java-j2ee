<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>
<div id="sidebar">

		<ul>

			<li><a href="LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="../PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	</div>
	<div id="main" style="height:620px">
	 	 
		<form name="homeForm">
	<center><font color="brown"><marquee><b>Welcome To NSEP</b></marquee></font></center><br></br>
<p> Employment is essential for lasting economic and social empowerment. Unfortunately the employment for non vocational under-graduate students 
can become an insurmountable barrier even if they had academically performed better. </p>
	<p> There is a deep reduction in number of undergraduates who get employment immediately after the graduation. 
	 Families from poorer backgrounds lack advance financial planning and savings which puts such expenses beyond their means for self employment.
	 Loans are not easily available for self employment for non vocational graduates. </p>
	<p> To address the above mentioned problem, National Self Employment Program for Under-graduate Students (NSEP) has come up with a unique solution
	  that will encourage the family and student to fund as much as they can and NSEP program to contribute the balance based on the predefined condition/rule. 
	
		</p>
		 
	    </form>
	 
	 

</div>



	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
