<%@page import="loanManagement.beans.Loan_Repayment_Schedule"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="./jsp/style.css" />
</head>
<body>
<div id="wrap">

	<div id="header"><img align="right" src="./jsp/logo.jpg" width="300" height="120" /><h1 style="top:15px;">National Self Employment Program</h1><br><br/><h1 style="top:55px;" >Loan Management</h1></div>

	<div id="nav">

		<ul>    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/home.jsp">Home</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/about_us.html">About Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

			<li><a href="./jsp/contact_us.html">Contact Us</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

		</ul>

	</div>
	<div id="sidebar" style="height:620px">

		<ul>

			<li><a href="./jsp/LoanRequest.jsp">New Loan Request</a></li><br/><hr/>
            <li><a href="./jsp/GenerateSchedule.jsp">Generate Repayment Schedule</a></li><br/><hr/>
			<li><a href="./PendingRequests">Create Loan Account</a></li><br/><hr/>
                        <li><a href="./jsp/loan_id.jsp">Loan Repayment</a></li><br/><hr/> 
                        <li><a href="./jsp/terms_conditions.html">Terms and Conditions</a></li><br/><hr/>  	 		 

		</ul>

	</div>
	<div id="main" style="height:620px">
	<div style=" POSITION: absolute; TOP: 300px;left:450px;">
	<center>
		<form name="scheduleView" style="top:320px;" action="./jsp/GenerateSchedule.jsp" method="post" >
		<%
		 ArrayList<Loan_Repayment_Schedule> getschedules=(ArrayList<Loan_Repayment_Schedule>) request.getAttribute("schedules");
		%>
		<table border="1">
		<tr >
		<td colspan="4">
		<center>Loan Request Number:<%= getschedules.get(0).getLoan_Request_No()%> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Student ID: <%= getschedules.get(0).getStudent_id()%></center>
		</tr>
		<tr>
		<td>
		<b>Principal</b>
		</td>
		<td>
		<b>Interest</b>
		</td>
		<td>
		<b>Month_Year</b>
		</td>
		</tr>
		<%
		    for(Loan_Repayment_Schedule schedule: getschedules)
		    {
		%>
		<tr>
		     
		    <td>
		    <%=schedule.getPrincipal() %>
		    </td>
		    <td>
		    <%=schedule.getInterest() %>
		    </td>
		    <td>
		    <%=schedule.getDeposit_month_Year() %>
		    </td>
		</tr>
		<% } %>
		<tr>
		<td  colspan="4">
		<center>
		<input style="width:80px" type="submit" name="ok" value="ok" />
		</center>
		</td>
		</tr>
		</table>
	    </form>
	</center>
	</div>

</div>
	<div id="footer">

		<center>Copyright @ 2011 Tata Consultancy Services</center>

	</div>

</div>
</body>
</html>
