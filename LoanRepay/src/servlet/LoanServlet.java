package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.LoanDao;

import bean.LoanBean;

/**
 * Servlet implementation class LoanServlet
 */
public class LoanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoanServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	String loan_id=request.getParameter("loan_id");
	LoanBean bean=new LoanBean();
	bean.setLoan_id(loan_id);
	System.out.println(loan_id);
	response.setContentType("text/html");
	PrintWriter pw = response.getWriter();
	
	try {
		new LoanDao().getData(bean);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	pw.println("<html>");
	pw.println("<body bgcolor=\"cyan\">");
	pw.println("<table border = 1>");
	pw.println("<td>"+"Student name"+"</td>");
	pw.println("<td>"+bean.getStudentname()+"</td>");
	pw.println("<td>"+"amount due"+"</td>");
	pw.println("<td>"+bean.getAmountdue()+"</td>");
	pw.println("<td>"+bean.getDuedate()+"</td>");
	pw.println("<td>"+bean.getStatus()+"</td>");
	pw.println("<td>"+"Status"+"</td>");
	pw.println("<td>"+bean.getLoan_id()+"</td>");
	pw.println("</tr>");
	pw.println("</body>");
	pw.println("</html>");

	
	}

}
