package bean;
import java.sql.Date;

public class LoanBean {

	private String studentname;
	private Date duedate;
	private int amountdue;
	private String status;
	private String loan_id;
	
	
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	
	
	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}
	
	public int getAmountdue() {
		return amountdue;
	}
	public void setAmountdue(int amountdue) {
		this.amountdue = amountdue;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getLoan_id() {
		return loan_id;
	}
	public void setLoan_id(String loanId) {
		loan_id = loanId;
	}
	
	
}
