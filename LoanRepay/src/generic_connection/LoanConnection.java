package generic_connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

public class LoanConnection
{

	public static Connection createConnection()
	{
		System.out.println("In connection");
		Connection con=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl="jdbc:oracle:thin:@INGNRILPINFM01:1521:ORCL";
			String dbusername="a50core";
			String dbpassword="a50core";
			//Get connection Object
			con=DriverManager.getConnection(dburl, dbusername, dbpassword);
			con.setAutoCommit(false);
	}
		catch (Exception e)
		{
        	System.out.println("Exception occurred while creating a connection and the type of exception is:"+e);
		}
		System.out.println("out connection");
		return con;
	}
	
	
	public static void closeAll(Connection con,PreparedStatement prepstmt,ResultSet rs)
	{
		
		try
		{
			if(rs!=null)
				rs.close();
			if(prepstmt!=null)
				prepstmt.close();
			if(con!=null)
				con.close();
		}
		
		catch(Exception e)
		{
			System.out.println("Exception while cleaning up resources in testPreparedStatement");
		}
		
	}
	
	
}
