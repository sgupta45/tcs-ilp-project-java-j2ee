package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import bean.LoanBean;

import generic_connection.LoanConnection;

public class LoanDao 
{
     Connection con=null;
	PreparedStatement stmt=null;
	ResultSet rs=null;
	

	public void getData(LoanBean bean) throws SQLException
	{
		//LoanConnection objconnec=new LoanConnection();
	 con= LoanConnection.createConnection(); 
	 System.out.println("In Dao");
	 String query="Select * from LoanRepay where loan_id= '"+bean.getLoan_id()+"'";
	 stmt=con.prepareStatement(query);
	 rs=stmt.executeQuery();
	 while(rs.next())
	 {
		 System.out.println("resultSet");
		 bean.setStudentname(rs.getString(1));
		 bean.setAmountdue(rs.getInt(2));
		 bean.setDuedate(rs.getDate(3));
		 bean.setStatus(rs.getString(4));
		 bean.setLoan_id(rs.getString(5));
		 
	 }
	 LoanConnection.closeAll(con, stmt, rs);
		
	}
	
}
